#include <fabui/wamp/auth/ticket.hpp>

using namespace fabui;
using namespace wampcc;

WampTicket::WampTicket(const std::string& provider_name)
	: WampAuth(provider_name)
{

}

auth_provider WampTicket::provider() {
	auto auth = auth_provider {
		// provider_name
		[this](const std::string&){
			return m_provider_name;
		},
		// policy
		[this](const std::string& authid, const std::string& realm) {
			//return auth_provider::auth_plan{ auth_provider::mode::open, {} };
			return onPolicy(authid, realm);
		},
		// cra_salt
		nullptr,
		// cra_check
		nullptr,
		// user_sercer
		nullptr,
		// user_role
		nullptr,
		// authorize
		[this](const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                auth_provider::action action){
			return onAuthorize(realm, authrole, uri, action);
		},
		// authenticate
		[this](const std::string& authid, // not an error, it's authid, realm in wampcc
                const std::string& realm,
                const std::string& authmethod,
                const std::string& signiture){
			return onAuthenticate(realm, authid, authmethod, signiture);
		},
		// hello
		nullptr
	};

	return std::move(auth);
}

auth_provider::auth_plan WampTicket::onPolicy(const std::string& /*authid*/, const std::string& /*realm*/) {
	return auth_provider::auth_plan(auth_provider::mode::authenticate, {"ticket"} );
}

auth_provider::authorized WampTicket::onAuthorize(const std::string& /*realm*/, 
	const std::string& /*authrole*/, 
	const std::string& /*uri*/, auth_provider::action /*action*/) 
{
	return {true, auth_provider::disclosure::optional};
}
