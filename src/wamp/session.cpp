#include <fabui/wamp/session.hpp>
#include <mtrebi/thread-pool/ThreadPool.h>
#define FMT_HEADER_ONLY
#define FMT_STRING_ALIAS 1
#include <fmt/format.h>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

WampSession::WampSession(unsigned workers, bool use_log)
	: m_use_threadpool(workers > 0)
	, m_connected(false)
	
{
	//the_kernel = wampcc::kernel({}, logger::console());
	if(use_log) {
		the_kernel = std::unique_ptr<wampcc::kernel>( new kernel({}, logger::console()) );
	} else {
		the_kernel = std::unique_ptr<wampcc::kernel>( new kernel() );
	}
	

	if(m_use_threadpool) {
		m_workers = std::make_unique<ThreadPool>(workers);
		m_workers->init();
	}
}

WampSession::~WampSession() {
	if(m_use_threadpool) {
		m_workers->shutdown();
	}
}

void WampSession::rpc_registered(const std::string& procedure, registered_info info)
{
	/*std::cout 	<< "rpc registration "
				<< (info.was_error? "failed, " + info.error_uri : "success")
				<< std::endl << std::flush;
	if (info.was_error)
		ready_to_exit.set_value();*/
	if (info.was_error) {
		std::cout << "failed: " << info.error_uri << std::endl << std::flush;
		ready_to_exit.set_value();
	}
	else {
		std::cout << "registered: " << procedure << std::endl << std::flush;
	}
}

void WampSession::submit(std::function<void()> job) {
	if(m_use_threadpool) {
		m_workers->submit(job);
	} else {
		job();
	}
}

void WampSession::yield(wampcc::t_request_id id) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id);
}

void WampSession::yield(wampcc::t_request_id id, wampcc::json_array ja) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id, ja);
}

void WampSession::yield(wampcc::t_request_id id, wampcc::json_array ja, wampcc::json_object jo) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id, ja, jo);
}

void WampSession::yield(t_request_id id, json_object options) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id, options);
}

void WampSession::yield(t_request_id id, json_object options, json_array ja) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id, options, ja);
}

void WampSession::yield(t_request_id id, json_object options, json_array ja, json_object jo) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->yield(id, options, ja, jo);
}

void WampSession::error(wampcc::t_request_id id, std::string msg) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->invocation_error(id, msg);
}

void WampSession::error(wampcc::t_request_id id, std::string msg, wampcc::json_array ja) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->invocation_error(id, msg, ja);
}

void WampSession::error(wampcc::t_request_id id, std::string msg, wampcc::json_array ja, wampcc::json_object jo) {
	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->invocation_error(id, msg, ja, jo);
}

void WampSession::provide(const std::string& procedure, on_invocation_fn invocation_cb, json_object options) {
	if(!m_connected)
		throw std::runtime_error("no active session");

	if(m_use_threadpool) {
		m_session->provide(procedure, options, 
					[procedure, this](wamp_session&, registered_info info) {
						rpc_registered(procedure, info);
					},
					[invocation_cb, this](wamp_session&, invocation_info info) {
						m_workers->submit( std::bind(invocation_cb, info) );
					});
	} else {
		m_session->provide(procedure, options,
					[procedure, this](wamp_session&, registered_info info) {
						rpc_registered(procedure, info);
					},
					[invocation_cb](wamp_session&, invocation_info info) {
						invocation_cb(info);
					});
	}
}

std::future<subscribed_info> WampSession::subscribe(const std::string& topic, on_event_fn event_cb, json_object options) {
	m_subscribed_promise = std::promise<wampcc::subscribed_info>();
	auto fut = m_subscribed_promise.get_future();

	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->subscribe(topic, options,
		[this](wamp_session&, subscribed_info info) {
			m_subscribed_promise.set_value(info);
		},
		[event_cb](wamp_session&, event_info info) {
			event_cb(info);
		}
	);
	lock.unlock();

	return fut;
}

std::future<unsubscribed_info> WampSession::unsubscribe(wampcc::t_subscription_id id) {
	m_unsubscribe_promise = std::promise<wampcc::unsubscribed_info>();
	auto fut = m_unsubscribe_promise.get_future();

	std::unique_lock<std::mutex> lock(m_mutex_ws);
	m_session->unsubscribe(id,
		[this](wamp_session&, unsubscribed_info info) {
			m_unsubscribe_promise.set_value(info);
		}
	);
	lock.unlock();

	return fut;
}

// std::future<published_info> WampSession::publish(const std::string& topic, wampcc::wamp_args args, json_object options) {
void WampSession::publish(const std::string& topic, wampcc::wamp_args args, json_object options) {
	//auto fut = m_published_promise.get_future();

	std::unique_lock<std::mutex> lock(m_mutex_ws);
	/*m_session->publish(topic, options, args, 
		[this](wamp_session&, published_info info){
			m_published_promise.set_value(info);
		}
	);*/	
	m_session->publish(topic, options, args);
	lock.unlock();

	//return fut;
}

std::future<result_info> WampSession::call(const std::string& procedure, wampcc::wamp_args args, json_object options) {
	m_result_promise = std::promise<wampcc::result_info>();
	auto fut = m_result_promise.get_future();

	// std::cout << "$wamp.CALL: " << procedure << std::endl;

	try {
		// std::cout << "$wamp.ws locked" << std::endl;
		std::unique_lock<std::mutex> lock(m_mutex_ws);
		m_session->call(procedure, options, args, 
			[this](wamp_session&, result_info info){
				// std::cout << "$wamp.callback" << std::endl;
				m_result_promise.set_value(info);
			}
		);
		lock.unlock();
		// std::cout << "$wamp.ws unlocked" << std::endl;
	} catch(const std::exception& e) {
		try {
			m_result_promise.set_exception(std::current_exception());
		} catch(const std::exception& e) {
			std::cout << e.what() << std::endl;
		}
		
	}

	// std::cout << "$wamp.CALL return future" << std::endl;

	return fut;
}

void WampSession::join(const std::string& realm) {
	try {
		auto opened = m_session->hello(realm);
		if( opened.wait_for(std::chrono::seconds(3)) != std::future_status::ready ) {
			throw std::runtime_error("time-out during session join");
		}
		if (!m_session->is_open())
		 	throw std::runtime_error("realm logon failed: " + realm);
		onJoin();
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

void WampSession::join(const std::string& realm, std::vector< std::string > authmethods) {
	try {
		client_credentials cc;
		cc.realm = realm;
		cc.authmethods = authmethods;
		auto opened = m_session->hello(cc);
		if( opened.wait_for(std::chrono::seconds(3)) != std::future_status::ready ) {
			throw std::runtime_error("time-out during session join");
			//disconnect();
		}
		if (!m_session->is_open()) {
			//disconnect();
		 	throw std::runtime_error("realm logon failed: " + realm);
		}
		onJoin();
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

void WampSession::join(const std::string& realm, std::vector<std::string> authmethods, const std::string& authid, wampcc::json_object /*authextra*/) {
	try {
		client_credentials cc;
		cc.realm = realm;
		cc.authid = authid;
		cc.authmethods = authmethods;

		cc.challenge_fn = [this](const std::string& user,
				const std::string& authmethod,
				const json_object& extra) {
			return onChallenge(user, authmethod, extra);
		};


		auto opened = m_session->hello(cc);
		if( opened.wait_for(std::chrono::seconds(3)) != std::future_status::ready ) {
			throw std::runtime_error("time-out during session join");
		}

		/*if (!opened.get())
		 	throw std::runtime_error("realm logon failed: " + realm);*/
		if (!m_session->is_open()) {
			//disconnect();
			//return;
		 	throw std::runtime_error("realm logon failed: " + realm);
		}

		onJoin();
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

void WampSession::join(const std::string& realm, std::vector< std::string > authmethods, const std::string& authid, const std::string &secret, json_object /*authextra*/) {

	std::cout << "join with full credentials\n";

	try {
		client_credentials cc;
		cc.realm = realm;
		cc.authid = authid;
		cc.authmethods = authmethods;

		cc.secret_fn = [secret]() {
			return secret;
		};

		auto opened = m_session->hello(cc);
		if( opened.wait_for(std::chrono::seconds(3)) != std::future_status::ready ) {
			throw std::runtime_error("time-out during session join");
		}

		/*if (!opened.get())
		 	throw std::runtime_error("realm logon failed: " + realm);*/
		if (!m_session->is_open()) {
			//disconnect();
			//return;
		 	throw std::runtime_error("realm logon failed: " + realm);
		}

		onJoin();
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

std::string WampSession::onChallenge(const std::string& /*user*/, const std::string& /*authmethod*/, const wampcc::json_object& /*extra*/) {
	throw std::runtime_error("onChallenge no implemented");
}

void WampSession::onConnect() {
	std::cout << "onConnect\n";
	join(m_realm);
}

void WampSession::onDisconnect()
{

}

void WampSession::disconnect() {
	m_session->close().wait();
}

bool WampSession::connect(const std::string& address, unsigned port, const std::string& realm) {
	bool result = true;

	try {

		/* Create the wampcc kernel. */
		std::unique_ptr<tcp_socket> socket(new tcp_socket( the_kernel.get() ));
		socket->connect(address, port).wait_for(std::chrono::seconds(3));

		if (!socket->is_connected())
			throw std::runtime_error("connect failed");

		//std::promise<void> ready_to_exit;
		m_session = wamp_session::create<websocket_protocol>(
				the_kernel.get(),
				std::move(socket),
				[this](wamp_session&, bool is_open) {
					if(not is_open)
						try {
							std::cout << "session closed by server\n";
							onDisconnect();
							ready_to_exit.set_value();
						} catch (...) {
							// ignore promise already set error
						}
				}, {});

		m_realm = realm;
		m_connected = true;
		onConnect();

	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}

	return result;
}

bool WampSession::connect(const std::string& uri, const std::string& realm) {
	uri_parts parts = wampcc::uri_parts::parse(uri);
	return connect(parts.path, std::atoi(parts.port.c_str()), realm);
}

const wampcc::json_value& WampSession::getArgumentOrFail(
	const std::string& name, 
	const wampcc::json_object& args_dict, 
	const std::string& error_message) 
{
	auto arg = args_dict.find(name);
	if(arg == args_dict.end())
		throw std::runtime_error(fmt::format(error_message, name));
	return arg->second;
}

const  wampcc::json_value& WampSession::getArgumentOrFail(
	unsigned index,
	const wampcc::json_array& args_list, 
	const std::string& error_message)
{
	if(args_list.size() < index)
		throw std::runtime_error(fmt::format(error_message, index));

	return args_list[index];
}


std::string WampSession::getCallerIdOrFail(
	wampcc::json_object& details,
	const std::string& error_message) 
{

	auto authid = details.find("caller_authid");
	if( authid == details.end() )
		throw std::runtime_error(error_message);
	return authid->second.as_string().c_str();
}
