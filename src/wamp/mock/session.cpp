#include <fabui/wamp/mock/session.hpp>

using namespace fabui;

WampSessionMock::WampSessionMock(bool verbose)
	: m_sub_id(0)
	, m_verbose(verbose)
{

}

WampSessionMock::~WampSessionMock()
{
	for(auto fut : m_publish_futures)
		delete fut;
	for(auto fut : m_call_futures)
		delete fut;
}

std::future<void> WampSessionMock::getPublishMatchFuture(const std::string& topic, unsigned count, const std::string& sub_topic)
{
	auto request = new WampRequestFuture{
		topic,
		0,
		count,
		sub_topic,
		std::promise<void>()
	};
	m_publish_futures.push_back( request );
	return request->promise.get_future();
}

void WampSessionMock::publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object opts)
{
	m_published.push_back( {topic, args, opts} );

	std::string sub_topic;
	if(args.args_list.size() > 0) {
		sub_topic = args.args_list[0].as_string();
	}

	if(m_verbose)
		std::cout << "publish: " << topic <<  " => " << args.args_list << " , " << args.args_dict << std::endl;

	for(auto fut : m_publish_futures) {
		if(fut->topic == topic) {
			if(fut->sub_topic.empty() or (fut->sub_topic == sub_topic)) {

				if(++fut->count == fut->target_count) {
					fut->promise.set_value();
				}
			}
		}
	}

}

void WampSessionMock::publish_to_topic(const std::string& topic, wampcc::wamp_args args)
{
	if(m_verbose)
		std::cout << "publish(test): " << topic <<  " => " << args.args_list << " , " << args.args_dict << std::endl;

	for(auto& sub : m_subscribed) {
		if(sub.active and sub.topic == topic) {
			//std::cout << "subscriber: " << sub.id << " @ " << sub.topic << std::endl;
			sub.event_cb({
				sub.id,
				{},
				args,
				nullptr
			});
		}
	}
}

std::future<void> WampSessionMock::getCallMatchFuture(const std::string& procedure, unsigned count)
{
	auto request = new WampRequestFuture{
		procedure,
		0,
		count,
		"",
		std::promise<void>()
	};
	m_call_futures.push_back( request );
	return request->promise.get_future();
}

std::future<wampcc::result_info> WampSessionMock::call(const std::string& procedure, wampcc::wamp_args args, wampcc::json_object opts)
{
	m_called.push_back( {
		procedure,
		args,
		opts,
		std::promise<wampcc::result_info>()
	} );

	auto& promise = (m_called.rbegin())->promise;

	if(m_verbose)
		std::cout << "call: " << procedure <<  " => " << args.args_list << " , " << args.args_dict << std::endl;

	for(auto fut : m_call_futures) {
		if(fut->topic == procedure) {
			if(++fut->count == fut->target_count) {
				fut->promise.set_value();
			}
		}
	}

	for(auto& resp: m_call_responders) {
		if(resp.first == procedure) {
			promise.set_value( resp.second(args) );
			return promise.get_future();
		}
	}

	promise.set_value({});
	return promise.get_future();
}

void WampSessionMock::addCallResponder(const std::string& procedure, std::function<wampcc::result_info(wampcc::wamp_args args)> callback)
{
	m_call_responders[procedure] = callback;
}

std::future<wampcc::subscribed_info> WampSessionMock::subscribe(const std::string& topic, on_event_fn event_cb, wampcc::json_object opts)
{
	m_subscribed.push_back({
		m_sub_id,
		topic,
		event_cb,
		opts,
		true
	});
	// auto& promise = (m_subscribed.rbegin())->sub_promise;

	m_subcribe_promise = std::promise<wampcc::subscribed_info>();

	m_subcribe_promise.set_value({
		0,
		m_sub_id,
		false,
		"",
		nullptr
	});

	if(m_verbose)
		std::cout << "subscribed: " << topic <<  " => " << m_sub_id << std::endl;

	m_sub_id++;

	return m_subcribe_promise.get_future();
}

std::future<wampcc::unsubscribed_info> WampSessionMock::unsubscribe(wampcc::t_subscription_id id)
{
	m_unsubcribe_promise = std::promise<wampcc::unsubscribed_info>();

	bool error = true;

	for(auto& sub: m_subscribed) {
		if(sub.active and sub.id == id) {
			sub.active = false;
			error = false;
			m_unsubcribe_promise.set_value({
				0, false, "", nullptr
			});
		}
	}

	if(error) {
		m_unsubcribe_promise.set_value({
			0, true, "", nullptr
		});
	}

	return m_unsubcribe_promise.get_future();
}
