#include <fabui/wamp/router.hpp>

using namespace fabui;
using namespace wampcc;

WampRouter::WampRouter(unsigned port)
	: m_port(port)
	//: the_kernel({}, logger::console())
{
}

WampRouter::~WampRouter() {

}

void WampRouter::onJoin() {

}

void WampRouter::onDisconnect() {

}

bool WampRouter::listen() {
	auto auth = wampcc::auth_provider::no_auth_required();
	return listen( std::move(auth) );
}

bool WampRouter::listen(auth_provider auth) {
	bool result = true;
	try {

		wamp_router::listen_options lo;
		lo.node = "0.0.0.0";
		lo.service = std::to_string(m_port);

		the_kernel = std::make_unique<wampcc::kernel>(
		 	wampcc::config({}), 
		 	logger::console() 
		 );

		m_router = std::make_shared<wamp_router>(the_kernel.get());

		//auto auth = auth_provider::no_auth_required();

		auto fut = m_router->listen(std::move(auth), lo);
		if (auto ec = fut.get())
			throw std::runtime_error(ec.message());

		m_running = true;
		onJoin();

	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
	return result;
}

void WampRouter::disconnect() {
	m_running = false;
	onDisconnect();
	ready_to_exit.set_value();
}

void WampRouter::publish(const std::string& /*realm*/, const std::string& /*uri*/,
						const json_object& /*options*/, wamp_args /*args*/)
{
	if(!m_running)
		throw std::runtime_error("not started yet");
}

void WampRouter::callable(const std::string& realm, const std::string& procedure, 
						on_call_fn call_cb
						/*,wampcc::json_object options*/)
{
	if(!m_running)
		throw std::runtime_error("not started yet");

	m_router->callable(
		realm,
		procedure,
		[call_cb](wamp_router&, wamp_session& ws, call_info info) {
			call_cb( std::ref(ws), info);
		}
	);
}