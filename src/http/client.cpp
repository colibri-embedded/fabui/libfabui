#include <fabui/http/client.hpp>
#include <curl/curl.h>
#include <iostream>
#include <fstream>

using namespace fabui;
using namespace wampcc;

json_value HttpReponse::json() {
	auto json = json_decode(content.c_str());
	return std::move(json);
}

struct WriteThis {
	const char *readptr;
	size_t sizeleft;
};

size_t writeToStringCallback(void *ptr, size_t size, size_t nmemb, std::string* data) 
{
	data->append((char*) ptr, size * nmemb);
	return size * nmemb;
}

size_t writeToFileCallback(void *ptr, size_t size, size_t nmemb, std::ofstream* file) 
{
	size_t written = size * nmemb; //fwrite(ptr, size, nmemb, stream);
	file->write((const char*)ptr, written);
	return written;
}

/*static size_t readCallback(void *ptr, size_t size, size_t nmemb, void *userp)
{
	struct WriteThis *pooh = (struct WriteThis *)userp;

	if(size*nmemb < pooh->sizeleft) {
		*(char *)ptr = pooh->readptr[0]; // copy one single byte
		pooh->readptr++;                 // advance pointer
		pooh->sizeleft--;                // less data left
		return 1;                        // we return 1 byte at a time!
	}

	return 0;                          // no more data left to deliver
}*/

HttpClient::HttpClient(std::string url) {
	setBaseUrl(url);
}

void HttpClient::setBaseUrl(std::string url)
{
	if( url.length() > 0) {
		base_url = url;
	}
}

void HttpClient::updateHeaders() {
	m_headers.clear();
	for(auto &pair : m_headers_raw) {
		m_headers.push_back( pair.first + ": " + pair.second );
	}
}

void HttpClient::setHeader(const std::string key, const std::string& value) {
	m_headers_raw[key] = value;
	updateHeaders();
}

void HttpClient::removeHeader(const std::string key) {
	m_headers_raw.erase(key);
	updateHeaders();	
}

void HttpClient::clearHeaders() {
	m_headers_raw.clear();
	m_headers.clear();
}

HttpReponse HttpClient::method(HttpMethod http_method, 
	const std::string& sub_url, 
	const wampcc::json_value& json_data,
	const params_t& params,
	const headers_t& headers)
{
	std::string data = json_encode(json_data);
	return method(http_method, sub_url, data, params, headers);
}

HttpReponse HttpClient::method(
	HttpMethod http_method, 
	const std::string& sub_url, 
	const std::string& data,
	const params_t& /*params*/,
	const headers_t& headers)
{

	CURL *curl;
	curl = curl_easy_init();
	if(curl) {

		if(before_method_cb)
			before_method_cb();

		HttpReponse response;
		response.url = base_url + sub_url;
		/* curl 
			-H "Accept:application/json" 
			-H "Content-Type:application/json"  
			-i -X POST 
			http://127.0.0.1:8000/auth/token 
			-d '{"grant_type": "password", "username": "kesler.daniel@gmail.com", "password": "password"}'
		*/

		//std::cout << "URL: " << url << std::endl;

		//struct curl_slist *headers = NULL;
		/*headers = ::curl_slist_append(headers, "Accept: application/json");
		headers = ::curl_slist_append(headers, "Content-Type: application/json");
		headers = ::curl_slist_append(headers, "charsets: utf-8");*/
		struct curl_slist *request_headers = NULL;

		for(auto &header : m_headers) {
			request_headers = ::curl_slist_append(request_headers, header.c_str());
		}

		for(auto &header : headers) {
			request_headers = ::curl_slist_append(request_headers, header.c_str());
		}

		/*for(auto &param : params) {

		}*/

		//struct WriteThis post_data;

		//std::string data = json_encode(post);

		//post_data.readptr = data.c_str();
		//post_data.sizeleft = data.length();

		::curl_easy_setopt(curl, CURLOPT_URL, response.url.c_str());

		::curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeToStringCallback);
		::curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response.content);

		::curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
		::curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
		if(request_headers)
			::curl_easy_setopt(curl, CURLOPT_HTTPHEADER, request_headers);

		switch(http_method) {
			case HttpMethod::GET: {

				break;
			}
			case HttpMethod::POST: {
				curl_easy_setopt(curl, CURLOPT_POST, 1L);
				if(!data.empty())
							::curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str() );
				break;
			}
			case HttpMethod::PUT: {
				//curl_easy_setopt(curl, CURLOPT_PUT, 1L);
				curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
				if(!data.empty())
					::curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str() );
				break;
			}
			case HttpMethod::DELETE: {
				curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				break;
			}
			case HttpMethod::PATCH: {
				// TODO: implement patch
				break;
			}
		}
		// curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		//::curl_easy_setopt(curl, CURLOPT_READFUNCTION, readCallback);
		//::curl_easy_setopt(curl, CURLOPT_READDATA, &post_data);

		//::curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

		auto res = ::curl_easy_perform(curl);

		if( res == CURLE_OK ) {
    		response.status_code = 0;
    		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response.status_code);
    		return std::move(response);
		}
		::curl_easy_cleanup(curl);
		return HttpReponse{500, "", ""};
	} else {
		return HttpReponse{500, "", ""};
	}

}

HttpReponse HttpClient::get(const std::string& sub_url, const params_t& params, const headers_t& headers) {
	return method(HttpMethod::GET, sub_url, std::string(), params, headers);
}

HttpReponse HttpClient::post(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params, const headers_t& headers) {
	return method(HttpMethod::POST, sub_url, json_data, params, headers);
}

HttpReponse HttpClient::put(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params, const headers_t& headers) {
	return method(HttpMethod::PUT, sub_url, json_data, params, headers);
}

HttpReponse HttpClient::delete1(const std::string& sub_url, const params_t& params, const headers_t& headers) {
	return method(HttpMethod::DELETE, sub_url, std::string(), params, headers);	
}