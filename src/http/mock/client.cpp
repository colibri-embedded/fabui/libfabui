#include <fabui/http/mock/client.hpp>
#include <curl/curl.h>
#include <iostream>
#include <fstream>

using namespace fabui;
using namespace wampcc;

HttpClientMock::HttpClientMock()
{

}

HttpClientMock::~HttpClientMock()
{

}

HttpReponse HttpClientMock::method(HttpMethod method, const std::string& sub_url, const std::string& data, const params_t& params, const headers_t& headers) 
{
	for(auto& resp: m_responder) {
		if(resp.sub_url == sub_url) {
			return resp.method(method, sub_url, data, params, headers);
		}
	}
	return HttpReponse{500, "{}", ""};
}

HttpReponse HttpClientMock::method(HttpMethod http_method, 
	const std::string& sub_url, 
	const wampcc::json_value& json_data,
	const params_t& params,
	const headers_t& headers)
{
	std::string data = json_encode(json_data);
	return method(http_method, sub_url, data, params, headers);
}

HttpReponse HttpClientMock::get(const std::string& sub_url, const params_t& params, const headers_t& headers) 
{
	return method(HttpMethod::GET, sub_url, std::string(), params, headers);
}

HttpReponse HttpClientMock::post(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params, const headers_t& headers) 
{
	return method(HttpMethod::POST, sub_url, json_data, params, headers);
}

HttpReponse HttpClientMock::put(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params, const headers_t& headers) 
{
	return method(HttpMethod::PUT, sub_url, json_data, params, headers);
}

HttpReponse HttpClientMock::delete1(const std::string& sub_url, const params_t& params, const headers_t& headers) 
{
	return method(HttpMethod::DELETE, sub_url, std::string(), params, headers);	
}

void HttpClientMock::addResponder(const std::string& sub_url, on_method_fn method)
{
	m_responder.push_front({sub_url, method});
}