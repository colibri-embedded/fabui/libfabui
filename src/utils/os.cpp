// system
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <exception>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
// 3rdparty
#define FMT_HEADER_ONLY
#define FMT_STRING_ALIAS 1
#include <fmt/format.h>
// local
#include <fabui/utils/os.hpp>
#include <fabui/utils/string.hpp>

#include <iostream>
/**
 * Get a list of files contained in <dir_name> directory.
 *
 * @param dir_name Directory path.
 * @returns unique pinter to std::vector<str::string>
 */
uptrStrings utils::os::getDirectoryFiles(std::string dir_name) 
{
	uptrStrings files(new std::vector<std::string>);

    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir_name.c_str())) == NULL) {
        std::string msg = fmt::format("Cannot open {}", dir_name);
        throw std::system_error( 
        		errno, 
        		std::system_category(), 
        		msg);
    }

    while ((dirp = readdir(dp)) != NULL) {
    	std::string file_name = std::string(dirp->d_name);
    	if(file_name != "." && file_name != "..") {
    		files->push_back(file_name);
    	}
    }
    closedir(dp);

	return std::move(files);
}

/**
 * Join path elements and add the required separators
 *
 * @param   Individual path elements
 * @returns Path string
 */
std::string utils::os::pathJoin(std::initializer_list<std::string> paths)
{
    std::string full_path;
    for(auto path: paths) {
        if(path[0] != '/' and full_path[full_path.length()-1] != '/')
            full_path += '/';
        full_path += path;
    }
    return full_path;
}

/**
 * Return only the basename part of the provided filename.
 *
 * @param filename Full filename
 * @return basename
 */
std::string utils::os::baseName(std::string filename)
{
    auto segments = utils::str::split(filename, '/');
    if(segments.size() > 0) {
        return segments[segments.size()-1];
    }
    return "";
}

/**
 * Return only the directory part of the provided filename.
 *
 * @param filename Full filename
 * @return dirname
 */
std::string utils::os::dirName(std::string filename)
{
    auto found = filename.find_last_of('/');
    if(found == std::string::npos) {
        return filename;
    }
    return filename.substr(0, found);
}

/**
 * Check if the file or directory exitsts.
 *
 */
bool utils::os::exists(std::string path)
{
    struct stat sb;
    if (stat(path.c_str(), &sb) == -1) {
        return false;
    }
    return true;
}

/**
 * Remove a file.
 *
 */
bool utils::os::remove(std::string filename)
{
    return unlink(filename.c_str()) == 0;
}

/**
 *
 *
 */
bool utils::os::mkDir(std::string dirname, bool recursive, unsigned mode)
{
    //int mkdir(const char *path, mode_t mode);
    struct stat sb;
    auto dirs = utils::str::split(dirname, '/');
    std::string path;
    for(auto &dir : dirs) {
        path += "/" + dir;

        if (stat(path.c_str(), &sb) == 0) {
            if (!S_ISDIR (sb.st_mode)) {
                return false;
            }
        }

        if(!utils::os::exists(path) and recursive) {
            if( mkdir(path.c_str(), mode) < 0 ) {
                return false;
            }
        } else {
            return recursive;
        }
    }

    return true;
}