#include <fabui/utils/string.hpp>
#include <sstream>
#include <iterator>
#include <algorithm> 

std::vector<std::string> utils::str::split(const std::string &in, char sep) {
    std::string::size_type b = 0;
    std::vector<std::string> result;

    while ((b = in.find_first_not_of(sep, b)) != std::string::npos) {
        auto e = in.find_first_of(sep, b);
        result.push_back(in.substr(b, e-b));
        b = e;
    }
    return result;
}

std::string utils::str::join(const std::vector<std::string> &elements, std::string separator, std::string prefix, std::string suffix)
{
	std::string result;

	for(auto it = elements.begin(); it != elements.end(); ++it) {
	    if(std::next(it) == elements.end()) {
	        result += prefix + *it + suffix;
	    } else {
	    	result += prefix + *it + suffix + separator;
	    }
	}

	return result;
}

void utils::str::ltrim_ref(std::string &s) 
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
}

void utils::str::rtrim_ref(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    // std::string t = " \t\n\r\f\v";
    // s.erase(s.find_last_not_of(t) + 1);
}

void utils::str::trim_ref(std::string &s)
{
	utils::str::ltrim_ref(s);
	utils::str::rtrim_ref(s);
}

std::string utils::str::rtrim(std::string s)
{
	utils::str::rtrim_ref(s);
	return s;
}

std::string utils::str::ltrim(std::string s)
{
	utils::str::ltrim_ref(s);
	return s;
}

std::string utils::str::trim(std::string s)
{
	utils::str::trim_ref(s);
	return s;
}