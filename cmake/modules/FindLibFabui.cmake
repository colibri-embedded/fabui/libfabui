# This module tries to find libserialxx library and include files
#
# LIBFABUI_INCLUDE_DIR, path where to find libwebsockets.h
# LIBFABUI_LIBRARY_DIR, path where to find libwebsockets.so
# LIBFABUI_LIBRARIES, the library to link against
# LIBFABUI_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( LIBFABUI_INCLUDE_DIR fabui/fabui.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBFABUI_LIBRARIES fabui
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBFABUI_LIBRARY_DIR ${LIBFABUI_LIBRARIES} PATH )

SET ( LIBFABUI_FOUND "NO" )
IF ( LIBFABUI_INCLUDE_DIR )
    IF ( LIBFABUI_LIBRARIES )
        SET ( LIBFABUI_FOUND "YES" )
    ENDIF ( LIBFABUI_LIBRARIES )
ENDIF ( LIBFABUI_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBFABUI_LIBRARY_DIR
    LIBFABUI_INCLUDE_DIR
    LIBFABUI_LIBRARIES
)
