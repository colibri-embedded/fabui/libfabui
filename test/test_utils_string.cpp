#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <fabui/utils/string.hpp>

//using namespace utils::str;

SCENARIO("String split utils", "[utils][str][split]") {

	WHEN("A space separated string is provided") {

		std::string test = "ab cd efg";

		THEN("It is split correctly") {
			auto result = utils::str::split(test);

			REQUIRE( result.size() == 3 );
			REQUIRE( result[0] == "ab" );
			REQUIRE( result[1] == "cd" );
			REQUIRE( result[2] == "efg" );
		}
	}

}

SCENARIO("String trim utils", "[utils][str][trim]") {

	WHEN("A string with trailing spaces") {

		THEN("Trailing spaces are removed") {
			std::string test = "test string   ";
			utils::str::rtrim_ref(test);
			REQUIRE( test == "test string" );
		}
	}

	WHEN("A string with trailing spaces") {

		THEN("Trailing spaces are removed") {
			std::string test = "test string   ";
			REQUIRE( utils::str::rtrim(test) == "test string" );
		}
	}

	WHEN("A string with leading spaces") {

		THEN("Leading spaces are removed") {
			std::string test = "   test string";
			utils::str::ltrim_ref(test);
			REQUIRE( test == "test string" );
		}
	}

	WHEN("A string with leading spaces") {

		THEN("Leading spaces are removed") {
			std::string test = "   test string";
			REQUIRE( utils::str::ltrim(test) == "test string" );
		}
	}

	WHEN("A string with leading and trailing spaces") {

		THEN("Leading spaces are removed") {
			std::string test = "   test string  ";
			utils::str::trim_ref(test);
			REQUIRE( test == "test string" );
		}
	}

	WHEN("A string with leading and trailing spaces") {

		THEN("Leading spaces are removed") {
			std::string test = "   test string  ";
			REQUIRE( utils::str::trim(test) == "test string" );
		}
	}

}