#include <iostream>
#include <fabui/wamp/router.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

class A: public WampRouter {
	public:
		A(unsigned port) 
			: WampRouter(port)
		{

		}

		void terminate(wamp_session &caller, call_info info) {
			std::cout << "terminated\n";
			caller.result(info.request_id);
			disconnect();
		}

		void onJoin() {
			callable(
				"fabui", 
				"fabui.router.terminate",
				std::bind(&A::terminate, this, _1, _2)
				);
		}

		void onDisconnect() {
			std::cout << "Disconnecting...\n";
		}
};

int main() {
	A a(9000);

	if( a.listen() ) {
		auto finished = a.finished_future();
		finished.wait();
	}
	return 0;
}