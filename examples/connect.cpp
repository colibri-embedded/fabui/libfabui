#include <iostream>
#include <fabui/wamp/session.hpp>

using namespace fabui;

class A: public WampSession {
	public:
		A(): WampSession() {}

		void onJoin() {
			std::cout << "Joined\n";
			disconnect();
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "fabui");
	return 0;
}