#include <fabui/wamp/session.hpp>

using namespace fabui;

class A: public WampSession {
	public:
		A(): WampSession() {}

		void onJoin() {}
};

int main() {
	A a;
	return 0;
}