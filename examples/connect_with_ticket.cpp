#include <iostream>
#include <fabui/wamp/session.hpp>

using namespace fabui;

class A: public WampSession {
	public:
		A(): WampSession() {}

		void onJoin() {
			std::cout << "Joined\n";
			disconnect();

		}

		void onConnect() {
			std::cout << "onConnect\n";
			join( realm(), {"ticket"}, "peter", "secret-ticket");
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	if( a.connect("127.0.0.1", 9000, "fabui") ) {
		std::cout << "get future\n" << std::flush;
		auto finished = a.finished_future();
		finished.wait();	
	}

	//a.connect("ws://127.0.0.1:55555", "private_realm");

	return 0;
}