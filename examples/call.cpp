//std::function<void()>
#include <iostream>
#include <fabui/wamp/session.hpp>
#include <mtrebi/thread-pool/ThreadPool.h>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

class A: public WampSession {
	private:
		t_subscription_id m_sid;
	public:
		A(): WampSession() {}

		void onJoin() {
			std::cout << "Joined\n";
			auto info = call("fabui.test.terminate", {}, {});
			info.wait();
			auto result = info.get();

			std::cout << "was_error: " << result.was_error << std::endl;
			std::cout << "list: " << result.args.args_list << ", dict: " << result.args.args_dict << std::endl;

			disconnect();
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "fabui");
	return 0;
}