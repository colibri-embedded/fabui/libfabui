#include <iostream>
#include <fabui/wamp/router.hpp>
#include <fabui/wamp/auth/ticket.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

class TicketProvider : public WampTicket {
	public:
		TicketProvider()
		{

		}

		auth_provider::authenticated onAuthenticate(const std::string& realm, 
				const std::string& authid,
                const std::string& authmethod,
                const std::string& ticket) {

			std::cout << "Authenticated: (" << authid << ", " << ticket << ") on " << realm << std::endl;

			return {
				true,
				"api"
			};
		}
};

class A: public WampRouter {
	private:
		TicketProvider ticket;
	public:
		A(unsigned port) 
			: WampRouter(port)
		{

		}

		void terminate(wamp_session &caller, call_info info) {
			std::cout << "terminated\n";
			caller.result(info.request_id);
			disconnect();
		}

		void onJoin() {
			callable(
				"fabui", 
				"fabui.router.terminate",
				std::bind(&A::terminate, this, _1, _2)
				);
		}

		void onDisconnect() {
			std::cout << "Disconnecting...\n";
		}

		bool listen() {
			auth_provider auth = ticket.provider();
			return WampRouter::listen(auth);
		}
};

int main() {
	A a(9000);

	if( a.listen() ) {
		auto finished = a.finished_future();
		finished.wait();
	}
	return 0;
}