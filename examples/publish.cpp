//std::function<void()>
#include <iostream>
#include <fabui/wamp/session.hpp>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

class A: public WampSession {
	private:
		t_subscription_id m_sid;
	public:
		A(): WampSession() {}

		void onJoin() {
			std::cout << "Joined\n";
			/*auto fut = publish("fabui.test.topic", { {1,2,3} }, { {WAMP_ACKNOWLEDGE,true} } );
			fut.wait();*/
			publish("fabui.test.topic", { {1,2,3} } );
			disconnect();
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "fabui");
	return 0;
}