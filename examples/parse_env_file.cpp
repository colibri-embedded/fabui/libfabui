#include <fabui/parser/env.hpp>
#include <iostream>

using namespace fabui;

int main(int argc, char* argv[]) {
	auto env = parse_env_file(argv[1]);

	for(auto &pair : env) {
		std::cout << pair.first << " = [" << pair.second << "]" << std::endl;
	}
}