#include <iostream>
#include <fabui/wamp/session.hpp>
#include <mtrebi/thread-pool/ThreadPool.h>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

class A: public WampSession {
	private:
		t_subscription_id m_sid;
	public:
		A(): WampSession(0) {}

		
		void terminate(invocation_info info) {
			yield(info.request_id, {true});
			disconnect();
		}

		void on_topic(event_info info) {
			std::cout << "on-topic\n";
			std::cout << "list: " << info.args.args_list << ", dict: " << info.args.args_dict << std::endl;

			//unsubscribe(m_sid);
		}

		void onJoin() {
			std::cout << "Joined\n";
			provide("fabui.test.terminate", std::bind(&A::terminate, this, _1));

			auto info = subscribe("fabui.test.topic", std::bind(&A::on_topic, this, _1) );
			info.wait();
			m_sid = info.get().subscription_id;
			std::cout << "subscribed: " << m_sid << std::endl;
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "fabui");
	return 0;
}