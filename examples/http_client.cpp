#include <fabui/http/client.hpp>

using namespace fabui;
using namespace wampcc;

int main() {
	HttpClient http("http://127.0.0.1:8000");

	http.setHeader("Accept", "application/json");
	http.setHeader("Content-Type", "application/json");
	http.setHeader("charsets", "utf-8");

	// Get autheorization token
	auto response = http.post("/auth/token", json_object{
	 	{ "grant_type", "password"},
	 	{ "username", "kesler.daniel@gmail.com"},
	 	{ "password", "password"}
	 });

	json_object content = response.json().as_object();
	std::cout << "status: " << response.status_code << std::endl;
	std::cout << "content: " << content << std::endl;

	// Build authorization header
	std::string auth_header = "Authorization: Bearer " + content["access_token"].as_string();

	// Get user identity
	response = http.get("/v1/me", {}, {auth_header});
	auto user = response.json().as_object();
	std::cout << "status: " << response.status_code << std::endl;
	std::cout << "user: " << user << std::endl;

	// Add auth header to all future requests
	http.setHeader("Authorization", "Bearer " + content["access_token"].as_string());

	// Create a new task
	response = http.put("/v1/tasks", json_object{
		{"user_id", 1},
		{"type", "custom"},
		{"controller", "make-custom"}
	});
	auto task = response.json().as_object();
	std::cout << "status: " << response.status_code << std::endl;
	std::cout << "task: " << task << std::endl;

	// Build task resource url
	std::string task_url = "/v1/tasks/" + std::to_string(task["id"].as_int());

	// Update task status
	response = http.post(task_url, json_object{
		{"status", "FINISHED"},
	});

	// Delete the task
	response = http.delete1(task_url);
	auto deleted = response.json().as_object();
	std::cout << "status: " << response.status_code << std::endl;
	std::cout << "deleted: " << deleted << std::endl;

	return 0;
}