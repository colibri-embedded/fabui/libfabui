#include <fabui/wamp/backend_service.hpp>

using namespace fabui;
using namespace wampcc;

class A: public BackendService {
	public:
		A() 
			: BackendService("example", "/mnt/projects/Colibri-Embedded/fabui/fabui-frontend/application/.env")
		{

		}

		void onJoin() {

		}
};

int main() {
	A a;
	if( a.connect("ws://127.0.0.1:9000", "fabui")) {
		auto finished = a.finished_future();
		finished.wait();
	}
}