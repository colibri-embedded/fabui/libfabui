#ifndef FABUI_PARSER_ENV_HPP
#define FABUI_PARSER_ENV_HPP

#include <string>
#include <map>

namespace fabui {

std::map<std::string,std::string> parse_env_file(const std::string& filename);

} // namsepace fabui

#endif /* FABUI_PARSER_ENV_HPP */