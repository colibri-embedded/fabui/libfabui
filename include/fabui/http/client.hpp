/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: client.hpp
 * @brief HttpClient definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef FABUI_HTTP_CLIENT_H
#define FABUI_HTTP_CLIENT_H

#include <string>
#include <map>
#include <wampcc/wampcc.h>

#define HTTP_OK  		200
#define HTTP_CREATED  	201
#define HTTP_ACCEPTED	202
#define HTTP_NO_CONTENT	204

namespace fabui {
//typedef void (*http_progress_callback)(unsigned long total, unsigned long current);
//typedef void (*http_finished_callback)();

enum class HttpMethod {
	GET,
	POST,
	PUT,
	PATCH,
	DELETE
};

struct HttpReponse {
	long status_code;

	std::string content;
	std::string url;

	wampcc::json_value json();
};

class IHttpClient {
public:

	typedef std::vector< std::string > headers_t;
	typedef std::map< std::string, std::string > params_t;

	virtual HttpReponse method(HttpMethod http_method, 
		const std::string& sub_url, 
		const wampcc::json_value& json_data,
		const params_t& params,
		const headers_t& headers) =0;

	virtual HttpReponse method(HttpMethod method, 
		const std::string& sub_url, 
		const std::string& data,
		const params_t& params,
		const headers_t& headers) =0;

	virtual HttpReponse get(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {}) =0;
	virtual HttpReponse post(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {}) =0;
	virtual HttpReponse put(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {}) =0;
	virtual HttpReponse delete1(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {}) =0;
};

class HttpClient: public IHttpClient {
public:

	/// Create HttpClient
	HttpClient(std::string url = "");

	/**
	 * 
	 */
	void setBaseUrl(std::string url);

	//std::string gql_query(std::string suburl, std::string data);
	//bool downloadFile(std::string &url, std::string &fileName);

	HttpReponse method(HttpMethod http_method, 
		const std::string& sub_url, 
		const wampcc::json_value& json_data = {},
		const params_t& params = {},
		const headers_t& headers = {});

	/**
	 * 
	 */
	HttpReponse method(HttpMethod method, 
		const std::string& sub_url, 
		const std::string& data,
		const params_t& params = {},
		const headers_t& headers = {});

	/**
	 * 
	 */
	HttpReponse get(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {});
	
	/**
	 * 
	 */
	HttpReponse post(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {});
	
	/**
	 * 
	 */
	HttpReponse put(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {});
	
	/**
	 * 
	 */
	HttpReponse delete1(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {});
	
	//http_progress_callback progressCallback;
	//http_finished_callback finishedCallback;
	std::function<void()> before_method_cb;

	/**
	 * 
	 */
	void setHeader(const std::string key, const std::string& value);

	/**
	 * 
	 */
	void removeHeader(const std::string key);

	/**
	 * 
	 */
	void clearHeaders();
private:
	/**
	 * 
	 */
	void updateHeaders();

	std::string base_url;

	std::map<std::string, std::string> m_headers_raw;
	headers_t m_headers;
};

} // namespace fabui

#endif /* FABUI_HTTP_CLIENT_H */
