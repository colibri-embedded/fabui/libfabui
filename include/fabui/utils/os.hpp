#ifndef UTILS_OS_HPP
#define UTILS_OS_HPP

#include <string>
#include <vector>
#include <memory>

typedef std::vector<std::string> Strings;
typedef std::shared_ptr<std::vector<std::string>> sptrStrings;
typedef std::unique_ptr<std::vector<std::string>> uptrStrings;

namespace utils {
	namespace os {

	uptrStrings getDirectoryFiles(std::string dir_name);

	bool exists(std::string path);
	bool remove(std::string path);
	// directory
	bool mkDir(std::string dirname, bool recursive=false, unsigned mode=0755);
	// path
	std::string pathJoin(std::initializer_list<std::string> paths);
	std::string baseName(std::string filename);
	std::string dirName(std::string filename);

}}

#endif /* UTILS_OS_HPP */