#ifndef UTILS_STRING_HPP
#define UTILS_STRING_HPP

#include <string>
#include <memory>
#include <vector>

namespace utils {
	namespace str {

	std::vector<std::string> split(const std::string &in, char sep=' ');

	std::string join(	const std::vector<std::string> &elements, 
						std::string separator="", 
						std::string prefix="", 
						std::string suffix="");

	void trim_ref(std::string&);
	void rtrim_ref(std::string&);
	void ltrim_ref(std::string&);

	std::string trim(std::string);
	std::string rtrim(std::string);
	std::string ltrim(std::string);

}}

#endif /* UTILS_STRING_HPP */