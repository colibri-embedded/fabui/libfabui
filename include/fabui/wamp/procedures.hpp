#ifndef WAMP_FABUI_PROCEDURES_HPP
#define WAMP_FABUI_PROCEDURES_HPP

#define PROCEDURE_MACRO_RUN "gcode.macro.run"
#define PROCEDURE_FILE_SET_NOTIFY_PARAMS "gcode.file.set_notify_params"

#define PROCEDURE_FILE_LOAD  "gcode.file.load"
#define PROCEDURE_FILE_PUSH  "gcode.file.push"
#define PROCEDURE_FILE_INFO  "gcode.file.info"
#define PROCEDURE_FILE_ABORT "gcode.file.abort"

#endif /* WAMP_FABUI_PROCEDURES_HPP */
