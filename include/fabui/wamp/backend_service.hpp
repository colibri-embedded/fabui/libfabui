#ifndef FABUI_WAMP_BACKEND_SERVICE_HPP
#define FABUI_WAMP_BACKEND_SERVICE_HPP

#include <chrono>
#include <fabui/wamp/session.hpp>
#include <fabui/http/client.hpp>

namespace fabui {

class BackendService : public fabui::WampSession {
	public:
		BackendService(const std::string& name, const std::string& env_file, unsigned workers = 0, bool use_log = false);
		
		HttpClient http;
	protected:
		void updateToken();
		void onConnect();
		std::string onChallenge(const std::string& user, const std::string& authmethod,	const wampcc::json_object& extra);
	private:
		std::string m_name;
		std::string m_key;
		std::string m_alg;

		std::string m_token;
		std::chrono::system_clock::time_point m_token_expires_at;

		void setKey(const std::string& key);
};

} // namespace fabui

#endif /* FABUI_WAMP_BACKEND_SERVICE_HPP */