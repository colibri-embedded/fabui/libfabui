#ifndef FABUI_WAMP_AUTH_HPP
#define FABUI_WAMP_AUTH_HPP

#include <wampcc/wampcc.h>

namespace fabui {

class WampAuth {
	public:
		WampAuth(const std::string& provider_name): m_provider_name(provider_name) { };
		~WampAuth() {};

		virtual wampcc::auth_provider provider() =0;
	protected:
		std::string m_provider_name;

};

} // namespace fabui

#endif /* FABUI_WAMP_AUTH_HPP */