#ifndef FABUI_WAMP_AUTH_ANONYMOUS_HPP
#define FABUI_WAMP_AUTH_ANONYMOUS_HPP

#include <fabui/wamp/auth.hpp>

namespace fabui {

class WampAnonymous: public WampAuth {
	public:
		WampAnonymous(const std::string& provider_name = "anonymous");
		wampcc::auth_provider provider();
};

} // namespace fabui

#endif /* FABUI_WAMP_AUTH_ANONYMOUS_HPP */