#ifndef FABUI_WAMP_AUTH_WAMPCRA_HPP
#define FABUI_WAMP_AUTH_WAMPCRA_HPP

#include <fabui/wamp/auth.hpp>

namespace fabui {

class WampWampcra: public WampAuth {
	public:
		WampWampcra(const std::string& provider_name = "wampcra");
		wampcc::auth_provider provider();
};

} // namespace fabui

#endif /* FABUI_WAMP_AUTH_WAMPCRA_HPP */