#ifndef FABUI_WAMP_AUTH_TICKET_HPP
#define FABUI_WAMP_AUTH_TICKET_HPP

#include <fabui/wamp/auth.hpp>

namespace fabui {

class WampTicket: public WampAuth {
	public:
		WampTicket(const std::string& provider_name = "ticket");
		wampcc::auth_provider provider();

		virtual wampcc::auth_provider::authenticated
			onAuthenticate(
				const std::string& realm,
                const std::string& authid,
                const std::string& authmethod,
                const std::string& ticket
			) =0;

		virtual wampcc::auth_provider::authorized
			onAuthorize(
				const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                wampcc::auth_provider::action
			);

		virtual wampcc::auth_provider::auth_plan 
			onPolicy(
				const std::string& authid, 
				const std::string& realm
			);
};

} // namespace fabui

#endif /* FABUI_WAMP_AUTH_TICKET_HPP */