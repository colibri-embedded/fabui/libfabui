#ifndef FABUI_WAMP_SESSSION_MACK_HPP
#define FABUI_WAMP_SESSSION_MACK_HPP

#include <fabui/wamp/session.hpp>

namespace fabui {

struct WampRequestFuture {
	std::string topic;
	unsigned count;
	unsigned target_count;
	std::string sub_topic;
	std::promise<void> promise;
};

struct WampPublishRequest {
	std::string topic;
	wampcc::wamp_args args;
	wampcc::json_object opts;
};

struct WampCallRequest {
	std::string topic;
	wampcc::wamp_args args;
	wampcc::json_object opts;
	std::promise<wampcc::result_info> promise;
};

struct WampSubscribeRequest {
	wampcc::t_subscription_id id;
	std::string topic;
	on_event_fn event_cb;
	wampcc::json_object opts;
	bool active;
};

class WampSessionMock : public IWampSession {
	private:
		wampcc::t_subscription_id m_sub_id;
		bool m_verbose;
	public:
		/// Create WampSessionMock
		WampSessionMock(bool verbose=false);

		/// Descructor
		~WampSessionMock();

		/// Collection of registered publish futures
		std::vector<WampRequestFuture*> m_publish_futures;
		/// Collection of published messages
		std::vector<WampPublishRequest> m_published;

		/// Colletion of registered call futures
		std::vector<WampRequestFuture*> m_call_futures;
		/// Collection of called procedures
		std::vector<WampCallRequest> m_called;
		/// mock call responders
		std::map<std::string, std::function<wampcc::result_info(wampcc::wamp_args)> > m_call_responders;

		/// Collection of subscribtions
		std::vector<WampSubscribeRequest> m_subscribed;
		/// Subscribe promise
		std::promise<wampcc::subscribed_info> m_subcribe_promise;
		/// Unsubscribe promise
		std::promise<wampcc::unsubscribed_info> m_unsubcribe_promise;

		/**
		 * Get publish match future.
		 *
		 * @param topic Publish topic match
		 * @param count Publish count match
		 * @param sub_topic Publish sub-topic match
		 *
		 * @returns Publish match future
		 */
		std::future<void> getPublishMatchFuture(const std::string& topic, unsigned count = 1, const std::string& sub_topic="");

		/**
		 * Publish a message.
		 *
		 * @param topic Message topic
		 * @param args  Message arguments
		 * @param options Message publish options
		 */
		void publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object options = {});

		/**
		 * Publish a message to topic subscribers. Do not track publication.
		 *
		 * @param topic Message topic
		 * @param args  Message arguments
		 */
		void publish_to_topic(const std::string& topic, wampcc::wamp_args args);

		/**
		 *
		 */
		std::future<wampcc::result_info> call(const std::string& procedure,	wampcc::wamp_args args = {}, wampcc::json_object options = {});

		/**
		 * Get call match future.
		 */
		std::future<void> getCallMatchFuture(const std::string& procedure, unsigned count = 1);

		/**
		 * Add a call mock.
		 *
		 * @param procedure Procedure signiture
		 * @param callbacl  Procedure callback function
		 */
		void addCallResponder(const std::string& procedure, std::function<wampcc::result_info(wampcc::wamp_args args)> callback);

		/**
		 *
		 */
		std::future<wampcc::subscribed_info> subscribe(const std::string& topic, on_event_fn event_cb, wampcc::json_object options = {});

		/**
		 *
		 */
		std::future<wampcc::unsubscribed_info> unsubscribe(wampcc::t_subscription_id);
};


} // namespace fabui

#endif /* FABUI_WAMP_SESSSION_MACK_HPP */
